Description:

fdtype purpose is to check the type of a given file descriptor
from shell scripts in the most portable way possible, it simply uses fstat
to make the check.


Motivations:

On a GNU/Linux system, I needed to check if my script
stdin and stdout where connected to a pipe. AFAIK, I could
check this using readlink /dev/stdin or /dev/stdout
but this is not portable and feels too hackish.
The terminal check 'test -t 0' didn't fit my cases either, so I
reflected upon why there wasn't a program enabling shell
scripts to do such a simple task. I still can't find
an answer, but in meantime I wrote this little tool.


Usage and options:

    fdtype [-c|-d|-l|-p|-f|-s|-v|-h] [<fd>]                 
                                                            
      <fd> defaults to 0                                          
                                                            
      -c    test for character device                           
      -d    test for directory                                  
      -l    test for link                                       
      -p    test for pipe                                       
      -f    test for regular file                               
      -s    test for socket                                     
      -v    print version                                       
      -h    print help                                          
          

Examples: 

    > ls | fdtype -p && echo 'pipe detected on stdin'

    > ls | { fdtype -p 1 && echo 'pipe detected on stdout' 1>&2 ; } | echo random text


Installing:

    > git clone https://codeberg.org/phranz/fdtype
    > cd fdtype
    > make
    > sudo make install

License:

    MIT
