/********************************************************************************************
* Copyright 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* Permission is hereby granted, free of charge, to any person 
* obtaining a copy of this software and associated documentation files
* (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, 
* and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
*
* The above copyright notice and this permission notice 
* shall be included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
********************************************************************************************/


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>


#define HELP()                                                              \
    do {                                                                    \
        puts(                                                               \
            "Usage: fdtype [-c|-d|-l|-p|-f|-s|-v|-h] [<fd>]\n"              \
            "Test the type of a given <fd> file descriptor\n"               \
            "\n"                                                            \
            "<fd> defaults to 0\n"                                          \
            "\n"                                                            \
            "  -c    test for character device\n"                           \
            "  -d    test for directory\n"                                  \
            "  -l    test for link\n"                                       \
            "  -p    test for pipe\n"                                       \
            "  -f    test for regular file\n"                               \
            "  -s    test for socket\n"                                     \
            "  -v    print version\n"                                       \
            "  -h    print help\n"                                          \
            "\n"                                                            \
            "Example: ls | fdtype -p && echo 'pipe detected on stdin'");    \
    } while (0)


#define USAGE(stream)                                                   \
    do {                                                                \
        fputs("Usage: fdtype [-c|-d|-l|-p|-f|-s|-v|-h] [<fd>]\n", stream); \
    } while (0)


int main(int argc, char **argv) {
	int fd;
    
    if (argc < 2) {
        USAGE(stderr);
        exit(EXIT_FAILURE);
    }

    fd = argc == 3 ? atoi(argv[2]) : 0;

    struct stat s;

    if (fstat(fd, &s) < 0) {
        perror("fstat");
        exit(EXIT_FAILURE);
    }

    if (!strcmp(argv[1], "-c"))
        return !((s.st_mode & S_IFMT) == S_IFCHR);
    else if (!strcmp(argv[1], "-d"))
        return !((s.st_mode & S_IFMT) == S_IFDIR);
    else if (!strcmp(argv[1], "-l"))
        return !((s.st_mode & S_IFMT) == S_IFLNK);
    else if (!strcmp(argv[1], "-p"))
        return !((s.st_mode & S_IFMT) == S_IFIFO);
    else if (!strcmp(argv[1], "-f"))
        return !((s.st_mode & S_IFMT) == S_IFREG);
    else if (!strcmp(argv[1], "-s"))
        return !((s.st_mode & S_IFMT) == S_IFSOCK);
    else if (!strcmp(argv[1], "-v"))
        printf("%s\n", "v0.1-r2");
    else if (!strcmp(argv[1], "-h"))
        HELP();
    else {
        fprintf(stderr, "Unknown option: %s\n", argv[1]);
        USAGE(stderr);
        exit(EXIT_FAILURE);
    }
}
